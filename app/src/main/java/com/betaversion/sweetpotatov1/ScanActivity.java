package com.betaversion.sweetpotatov1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.betaversion.sweetpotatov1.api.model.MobileNumberDto;
import com.google.zxing.Result;
import com.google.zxing.integration.android.IntentIntegrator;

public class ScanActivity extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 1;
    private Button buttonScan;
    private IntentIntegrator qrScan;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        Log.i("_______________****", "Activity2");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        buttonScan = (Button) findViewById(R.id.buttonScan);
        buttonScan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent( ScanActivity.this, OpenScannerActivity.class);
                startActivity(i);

            }
        });

    }
}

