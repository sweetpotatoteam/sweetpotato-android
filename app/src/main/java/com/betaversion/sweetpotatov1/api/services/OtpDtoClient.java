package com.betaversion.sweetpotatov1.api.services;


import android.support.v4.media.AudioAttributesCompat;

import com.betaversion.sweetpotatov1.api.model.MobileNumberDto;
import com.betaversion.sweetpotatov1.api.model.OtpDto;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OtpDtoClient {

    @POST("v1/otp/validation")
    @Headers("Content-Type: application/json")
    Call<Boolean> validateOtp(@Body OtpDto otpDto);


    @POST("v1/otp")
    @Headers("Content-Type: application/json")
    Call<Void> sendOtp(@Body MobileNumberDto requestDto);


}
