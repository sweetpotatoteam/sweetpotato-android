package com.betaversion.sweetpotatov1;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.betaversion.sweetpotatov1.api.model.MobileNumberDto;
import com.betaversion.sweetpotatov1.api.model.OtpDto;
import com.betaversion.sweetpotatov1.api.services.OtpDtoClient;

import java.io.IOException;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;


public class MainActivity extends Activity {

    TextView sweetPotatoText;
    ImageView imgView;
    Button goButton;
    EditText editTextMobNo;
    private final String TAG = this.getClass().getSimpleName();
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        // Set the custom font
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Chalkduster.ttf");
        sweetPotatoText.setTypeface(type);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobileNumberDto MobileNumberDto = new MobileNumberDto(String.valueOf(editTextMobNo.getText()));
                sendNetworkRequest(MobileNumberDto);
            }
        });
    }

    /**
     * Initialize all view components
     */
    private void initViews() {
        this.sweetPotatoText = (TextView) findViewById(R.id.textView1);
        this.imgView = (ImageView)findViewById(R.id.imageView);
        this.editTextMobNo = (EditText)findViewById(R.id.mobNo);
        this.goButton = (Button) findViewById(R.id.go);
    }

    /**
     *http://10.0.2.2:8080/api/
     * @param mobileNumberDto
     */
    private void sendNetworkRequest(MobileNumberDto mobileNumberDto){
        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OtpDtoClient client = retrofit.create(OtpDtoClient.class);
        Call<Void> call = client.sendOtp(mobileNumberDto);


        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(final Call<Void> call, final Response<Void> response) {
              if(200== response.code()){
                  Toast.makeText(getApplicationContext(), "OTP Sent!" , Toast.LENGTH_LONG).show();
                  Intent i = new Intent(MainActivity.this, OTPActivity.class);
                  Bundle bundle = new Bundle();
                  bundle.putString("mobNo", String.valueOf(editTextMobNo.getText()));
                  i.putExtras(bundle);
                  startActivity(i);
              } else {
                  // @TODO: if exception do something here
              }
            }

            @Override
            public void onFailure(final Call<Void> call, final Throwable t) {
                Toast.makeText(getApplicationContext(), "Services are down!" , Toast.LENGTH_LONG).show();
                Log.e(TAG,t.getMessage());
            }
        });
    }
}
