package com.betaversion.sweetpotatov1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.betaversion.sweetpotatov1.api.model.OtpDto;
import com.betaversion.sweetpotatov1.api.services.OtpDtoClient;
import com.mukesh.OtpListener;
import com.mukesh.OtpView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OTPActivity extends AppCompatActivity {
    TextView sweetPotatoText;
    EditText editTextMobNo;
    OtpView otpView;
    Bundle bundle;
    private static Retrofit retrofit = null;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initViews();
        //set custom font
        Typeface type = Typeface.createFromAsset(getAssets(), "fonts/Chalkduster.ttf");
        sweetPotatoText.setTypeface(type);

       //disable Mob number
        editTextMobNo.setEnabled(false);

        //Fetch data from old activity
        bundle = getIntent().getExtras();
        editTextMobNo.setText(bundle.getString("mobNo"));

        otpView.setListener(new OtpListener() {

            @Override
            public void onOtpEntered(String otp) {
                retrofit = new Retrofit.Builder()
                        .baseUrl("http://10.0.2.2:8080/api/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                OtpDtoClient client = retrofit.create(OtpDtoClient.class);
                Call<Boolean> call = client.validateOtp(new OtpDto(otpView.getOTP(), bundle.getString("mobNo")));

                call.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(final Call<Boolean> call, final Response<Boolean> response) {
                        if (true == response.body().booleanValue()) {
                            Intent i = new Intent( OTPActivity.this, ScanActivity.class);
                            startActivity(i);
                        } else {
                            Intent i = new Intent(OTPActivity.this, MainActivity.class);
                            startActivity(i);
                        }
                    }

                    @Override
                    public void onFailure(final Call<Boolean> call, final Throwable t) {
                        Intent i = new Intent(OTPActivity.this, MainActivity.class);
                        startActivity(i);
                    }
                });
            }
        });
    }

    /**
     * Initialize the view
     */
    private void initViews() {
        this.sweetPotatoText = (TextView) findViewById(R.id.textView1);
        this.editTextMobNo = (EditText) findViewById(R.id.mobNo);
        this.otpView = (OtpView)findViewById(R.id.otpView);
    }
}
