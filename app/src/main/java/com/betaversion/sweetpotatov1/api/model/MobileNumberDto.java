package com.betaversion.sweetpotatov1.api.model;

public class MobileNumberDto {

    private String mobileNumber;

    public MobileNumberDto(final String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Deprecated
    private String[] mobileNumberGroup;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String[] getMobileNumberGroup() {
        return mobileNumberGroup;
    }

    public void setMobileNumberGroup(String[] mobileNumberGroup) {
        this.mobileNumberGroup = mobileNumberGroup;
    }
}
