package com.betaversion.sweetpotatov1.api.model;

import com.google.gson.annotations.SerializedName;

public class OtpDto {

    @SerializedName("otp")
    private String otp;

    @SerializedName("mobileNumber")
    private String mobileNumber;


    public OtpDto(final String otp, final String mobileNumber) {
        this.otp = otp;
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() { return mobileNumber; }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
